console.log('Hello World');

// Activity 1
function user2N() {	
	let no1 = parseInt(prompt('Input first number'));
	let no2 = parseInt(prompt('Input second number'));
	let sum = no1 + no2;
	let diff = no1 - no2;
	let prod = no1 * no2;
	let quo = no1 / no2;
	
	if (sum < 10) {
		console.warn('The sum of two numbers is: ' + sum);
	} else if (sum > 9 && sum < 21) {
		alert('The difference of two numbers is: ' + diff);
	} else if (sum > 20 && sum < 30) {
		alert('The product of two numbers is: ' + prod);
	} else {
		alert('The quotient of two numbers is: ' + quo);
	}
}

// Activity 2
function userInfo() {
	let name = prompt('What is your name?');
	//name = name.toLowerCase();
	let age = prompt('How old are you?');

	if (name === '' || null){
		alert('Are you a time traveller?');
	} else if (age === '' || null){
		alert('Are you a time traveller?');
	} else {
		alert(name + ' ' + age);
	}
}


//Activity 3
function checkUser() {
	let userAge = prompt('How old are you?'); //outcome type is string -> number

	let user;
	switch(userAge) {
		//declare multiple cases to represent each outcome that will match the value inside the expression.
		case '18':
			user = "You are now allowed to party.";
			break;
		case '21':
			user = "Your are now part of the adult society.";
			break;
		case '65':
			user = "We thank you for your contribution to society.";
			break;
		default:
			user = "Are you sure you're not an alien?";
			//if all else fails or if none of the declared options
	}
	alert(user);
}
//




//
function ageChecker() {
	//try-catch statement
	//get the input of the user
	//document parameter describes the HTML document/file where the JS module is linked
	//.value - describes the value property of our elements
	let userInput = document.getElementById('age').value;
	let message = document.getElementById('outputDisplay');
	/*syntax
	try{
		tryCode - Block of code to try
	}
	catch(err){
		catchCode - Block of code to handle errors
	}
	*/
	try {
		//make sure the input inserted by user is NOT equal to a blank string
		//throw -> statement that exmaines the input and returns an error
		if (userInput === '') throw 'The input is empty';
		//to check if value is NOT a number, we will use a isNaN()
		if (isNaN(userInput)) throw 'The input is not a number';

	} catch(err) {
		//the "err" is to define the error that will be thrown by the try section. and will be caught by catch statement to display custom error.
		// innerHTML property - to inject a value inside the HTML container
		//syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element.
		message.innerHTML = err;
	}	finally {
		//this statement here will be exectuted regardless of the result above
		alert('Finally');
	}
}
console.log('Hellow World')

/*SECTION - CONCTROL STRUCTURE
	:IF-ELSE STATEMENT	
*/
let numA = -1; //we will try to assess the value.
//If statement (Branch)
//THe task of the if statement is to execute a procedure action if the specified condition is "true"
if (numA <= 0) {
	//truthy branch
	//this block of code will run if the condition is met
	//console.log('The condition was MET!');
}

/*
let name = 'Lorenzo';

if (name === 'Lorenzo') {
	console.log('User can proceed!')
}

let isLegal = true;
if (isLegal) {
	console.log('User can proceed..');
}
*/
	//ELSE STATEMENT
	//	this executes a statement if ALL other conditions are FALSE and/or has failed

//PROMPT BOX -> prompt(): allows us to display a prompty dialog box which we can the user for an input.
	//*Syntax: prompt(text/message, placeholder/default text);
	//prompt("Please enter your first name:", "Alje");


/*
let age = 21;
//alert() => displas a message box to the user which would require their attention.
if (age >= 18) {
	alert("Your old enough to drink.");
} else {
	//Falsy branch
	alert("Come back another day.");
}
*/


//lets create a control structure that will ask for the number of drinks that you will order.
	//ask the user how many drinks he wants
	//🥂 getemoji.com


/*
let order = prompt('How many orders of drinks do you like?');
*/


	//parseInt() -> allows us to convert strings into integers
	//create a logic that will make sure that the user's input is greater than 0
/*
order = parseInt(order);
this code is not neede because of repeat
*/

// type coercion - conversion data type was converted to another data type.
	// if one of operands in an object, it will be converted into a primitive data type (str, number, boolean)
	// if atleast 1 operand is a string, it will convert the other operand into a string
	//  if both numbers are numbers then an arithmetic operation will be executed

/* 3 WAYS TO MUTIPLY A STRING:
	- repeat() method -> allows us to return a new string value that contains the number of copies of the string.
		* SYNTAX: str.repeat(value/number)

	- loops -> for loop method
	- loops -> while loop method
*/


/*
if (order > 0) {
	console.log(typeof order);
	let cocktail = "🍺";
	alert(cocktail.repeat(order)); //using repeat method
} else {
	alert('The number should be above 0')
}
*/


//mini task: vaccine checker
function vaccineChecker(){
	//ask information from the user
	let vax = prompt('What brand is your vaccine?');
	//pfizer, PFIZER
	//So we need to process the input of the user so that whatever input he will enter we can control the uniformity of the character casing.
	//toLowerCase() -> allows us to convert a string into all lowercase characters.
	//SYNTAX: string.toLowerCase()
	vax = vax.toLowerCase();
	//create a logic that will allow us to check the values inserted by the user to match the given parameters.
	if (vax === 'pfizer' || vax === 'moderna' || vax === 'astrazeneca' || vax === 'janssen'){
		//display the response back to the client.
		alert(vax + ' is allowed to travel');
	} else {
		alert('sorry ' + vax + ' not permitted');
	}
}
//we want the user to invoke this function with the use of a trigger
//vaccineChecker();
//onclick -> is an example of a JS Event (needs to be added in html)
//syntax: <element/component onclick="myScript/Function">

//Typhoon checker
function determineTyphoonIntensity(){
	//going to need an input from the user.
	//we need a 'number' data type so that we can properly compare the values
	let windspeed = parseInt(prompt('Wind speed:'));
	console.log(typeof windspeed); //this is to prove that we can directly pass the value of the variable and feed to the parseInt method

	//note: "else if" is 2 words
	/*
	if (windspeed < 30) {
		alert('Not a Typhoon yet!');
	} else {
		alert('Typhoon Detected');
	}
	*/
	if (windspeed <= 29) {
		alert('Not a Typhoon yet!');
	} else if (windspeed <= 65) {
		//this will run if 2nd statement was met
		alert('Tropical Depression Detected');
	} else if (windspeed <= 88) {
		alert('Tropical Storm Detected');
	} else if (windspeed <= 117) {
		alert('Severe Tropical Storm');
	}
	else {
		alert('Typhoon Detected');
	}
	
}

/*CONDITIONAL TERNARY
	- still follows the same syntax with an if-else
	- truthy, falsy
	- this is the only JS operator that takes 3 operands
	- SYNTAX:
		condition ? "truthy" : falsy
		? Question Mark -> describes a condition that if resulted to TRUE, will run truthy
		: Colon -> describes falsy
*/
function ageChecker() {
	let age = parseInt(prompt('How old are you?'));

	return (age >= 18)? alert('Old enough to vote') : alert('Not yet old enough');

	/*simplify the code below with help of ternary operator
	if (age >= 18) {
		//truthy
		alert('Old enough to vote');
	} else {
		//falsy
		alert('Not yet old enough');
	}
	*/
}	


/* SWITCH STATEMENTS
switch(expression) {
	case x:
		//code block
	break;
	case y:
		//code block
	break;
	default:
		//code block
}
*/

function determineComputerOwner() {
	let unit = prompt('What is the unit no. ?'); //outcome type is string -> number

	let user;
	//the unit -> represents the unit number.
	//the user -> represents the user who owns the unit.
	switch(unit) {
		//declare multiple cases to represent each outcome that will match the value inside the expression.
		case '1':
			user = "Randy Blythe";
			break;
		case '2':
			user = "Chris Adler";
			break;
		case '3':
			user = "Mark Morton";
			break;
		case '4':
			user = "Willie Adler";
			break;
		case '5':
			user = "John Campbell";
			break;
		default:
			user = 'Not a valid option';
			//if all else fails or if none of the declared options
	}
	console.log(user);
}
//determineComputerOwner();